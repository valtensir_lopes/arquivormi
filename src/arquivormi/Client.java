package arquivormi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class Client {
    
    public static void main(String[] args)throws NotBoundException, MalformedURLException, RemoteException {
        Service service = (Service) Naming.lookup("rmi://localhost:5099/conectionArquivo");
        
        Scanner leitor = new Scanner(System.in);
        String nomeArquivo;
        
        System.out.println("Digite o nome do arquivo que deseja procurar:");
        nomeArquivo = leitor.next();
        
        if (service.localizaArquivo(nomeArquivo)) {
            System.out.println("Arquivo localizado!");
        } else {
            System.out.println("Arquivo não localizado!");
        }
        
        
    }
    
    
}
